# Provide inside Quasar boot file:
I want to use [Syncfusion](https://www.syncfusion.com/) grid component with [Quasar2](https://quasar.dev/).

I've created a boot file to import and make this component available everywhere. But it also needs a provide statement that I had to code at `Vue.app`.

I wish I could move this provide code to boot file, so everything is keep together.

According to [SyncFusion instruction](https://ej2.syncfusion.com/vue/documentation/getting-started/vue3-tutorial/) their components make use of `vue-class-component (8.0.0-rc.1)`. So I just added it to `package.json` (didn´t make any other change).

During compilation there is a warning: `export 'default' (imported as 'Vue') was not found in 'vue' (possible exports: blah blah blah...`

But even with this warning the component works as expected.

I don´t know the effects this warning can cause, nor if it can be related with provide...


# Hot to reproduce:

In terminal:
```bash
git clone https://gitlab.com/botura/quasar2-syncfusion.git
cd quasar2-syncfusion
yarn
quasar dev
```

### 1) js-grid works and columns can be grouped

### 2) change App.vue:
  *  comment line 6: `import { Group } from...`
  *  comment line 13: `grid: [Group]`

### 3) js-grid columns can not be grouped, even the provide is also included at `boot/syncfusion.js`
```

