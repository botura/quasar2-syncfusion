import { boot } from 'quasar/wrappers'
// import { GridComponent, ColumnsDirective, ColumnDirective } from '@syncfusion/ej2-vue-grids'
import { GridComponent, ColumnsDirective, ColumnDirective, Group } from '@syncfusion/ej2-vue-grids'

export default boot(({ app }) => {
  app.component('ejs-grid', GridComponent)
  app.component('e-columns', ColumnsDirective)
  app.component('e-column', ColumnDirective)

  // this line has no effect
  app.provide('grid', [Group])
})
